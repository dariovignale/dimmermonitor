/*
 * i2cHL.h
 *
 *  Created on: 02/ott/2012
 *      Author: Genesy
 */

#ifndef I2CHL_H_
#define I2CHL_H_

#include <stdint.h>
#include "driver_config.h"
#include "target_config.h"

#include "type.h"
#include "i2c.h"



//ritornano lo stato dell'operazione
uint32_t I2CWrite(uint8_t slaveAddress, uint16_t intAddress, uint8_t nByteAddress, uint8_t nByte, uint8_t* data);

uint32_t I2CRead(uint8_t slaveAddress, uint16_t intAddress, uint8_t nByteAddress, uint8_t nByte, uint8_t* data);



#endif /* I2CHL_H_ */
