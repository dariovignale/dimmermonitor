/*
 * i2cHL.c
 *
 *  Created on: 02/ott/2012
 *      Author: Genesy
 */

#include "i2cHL.h"
#include <string.h>



extern volatile uint8_t I2CMasterBuffer[BUFSIZE];
extern volatile uint8_t I2CSlaveBuffer[BUFSIZE];

extern volatile uint32_t I2CReadLength, I2CWriteLength;

uint32_t I2CWrite(uint8_t slaveAddress, uint16_t intAddress, uint8_t nByteAddress, uint8_t nByte, uint8_t* data){
	  I2CWriteLength = nByte + 1 + nByteAddress;
	  I2CReadLength = 0;
	  I2CMasterBuffer[0] = slaveAddress;
	  uint8_t n = nByteAddress;
	  I2CMasterBuffer[n] = (uint8_t)intAddress;
	  while ((n--) > 1)
	 		  I2CMasterBuffer[n] = (uint8_t)(intAddress >> 8);
	  int i;
	  for(i=0; i<nByte; i++){
		  I2CMasterBuffer[i + 1 + nByteAddress] = data[i];
	  }
	  uint32_t state = I2CEngine();
  return state;
}

uint32_t I2CRead(uint8_t slaveAddress, uint16_t intAddress, uint8_t nByteAddress, uint8_t nByte, uint8_t* data){
		int i;
		for ( i = 0; i < BUFSIZE; i++ )
		{
			I2CMasterBuffer[i] = 0x00;
			I2CSlaveBuffer[i] = 0x00;
		}
		I2CWriteLength = 1 + nByteAddress;
		I2CReadLength = nByte;
		I2CMasterBuffer[0] = slaveAddress;
		 uint8_t n = nByteAddress;
		I2CMasterBuffer[n] = (uint8_t)intAddress;
		while ((n--) > 1)
				  I2CMasterBuffer[n] = (uint8_t)(intAddress >> 8);
		I2CMasterBuffer[nByteAddress + 1] = slaveAddress | RD_BIT;
		uint32_t state = I2CEngine();
		memcpy(data,(const void*)I2CSlaveBuffer,nByte);
	return state;
}


