/*
 * utility_function.h
 *
 *  Created on: 09/nov/2012
 *      Author: Genesy
 */

#ifndef UTILITY_FUNCTION_H_
#define UTILITY_FUNCTION_H_

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "defines.h"


#define PI 3.14159265

uint16_t little_endian_buffer_to_uint16(uint8_t* buffer);
bool two_complement_extract_sign(int16_t data, uint8_t resolution);
uint16_t two_complement_sign_extension(uint16_t value, uint8_t resolution);
uint16_t two_complement_To_module_conv(int16_t data, uint8_t resolution);
MeasAngle_t convert_g_in_angle(MeasAccel_t value_in_g);
MeasAngle_t convert_digital_value_in_angle(uint16_t module, gRange g_range, uint8_t resolution);
MeasAccel_t convert_digital_value_in_g(int16_t value, uint8_t g_range, uint8_t resolution);

#endif /* UTILITY_FUNCTION_H_ */
