/*
 * defines.h
 *
 *  Created on: 24/set/2012
 *      Author: Emiliano
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#include <stdint.h>
#include "stddef.h"

typedef float 	MeasAngle_t;
typedef float 	MeasAccel_t;
typedef int16_t OUTAngle_t;
typedef uint16_t Period_t;
typedef enum { RANGE_2g = 4, RANGE_4g = 8, RANGE_8g = 16 } gRange ;

#ifndef FALSE
#define FALSE   (0)
#endif

#ifndef TRUE
#define TRUE    (1)
#endif


#define MAX_NUM_TIMER		10

#define __disableInterrupt()
#define __enableInterrupt()

#define ENABLE_INTERRUPT	__enableInterrupt()
#define DISABLE_INTERRUPT   __disableInterrupt()



#endif /* DEFINES_H_ */
