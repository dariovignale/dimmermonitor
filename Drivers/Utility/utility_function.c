/*
 * utility_function.c
 *
 *  Created on: 09/nov/2012
 *      Author: Genesy
 */

#include "utility_function.h"


uint16_t little_endian_buffer_to_uint16(uint8_t* buffer){
	uint16_t data = buffer[1] | (buffer[0] << 8);
	return data;
}

bool two_complement_extract_sign(int16_t data, uint8_t resolution){
	bool sign = (data >> 15); //resolution);
	return sign;
}


uint16_t two_complement_To_module_conv(int16_t data, uint8_t resolution){
	bool sign = two_complement_extract_sign(data, resolution);
	data = data >> (16 - resolution);
	if (sign != 0){
		data	= (~data + 1);
	}
	return data;
}

uint16_t two_complement_sign_extension(uint16_t value, uint8_t resolution){
	int i;
	int16_t tmp = value;
	bool sign = two_complement_extract_sign(value, resolution);
	for (i=resolution-1; i<16; i++){
		tmp |= (sign << i);
	}
	return tmp;
}


MeasAccel_t convert_digital_value_in_g(int16_t value, uint8_t g_range, uint8_t resolution){
	MeasAccel_t value_in_g;
	value = value / (pow(2, (16 - resolution)));
	value_in_g = value * g_range / pow(2,resolution);
	return value_in_g;
}

MeasAngle_t convert_g_in_angle(MeasAccel_t value_in_g){
	MeasAngle_t angle;
	angle = asin(value_in_g) * 180.0 / PI;
	return angle;
}

MeasAngle_t convert_digital_value_in_angle(uint16_t module, gRange g_range, uint8_t resolution){
	MeasAccel_t value_in_g = convert_digital_value_in_g(module, g_range, resolution);
	MeasAngle_t angle = convert_g_in_angle(value_in_g);
	return angle;
}
