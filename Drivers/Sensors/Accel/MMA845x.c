/*
 * MMA845x.c
 *
 *  Created on: 01/ott/2012
 *      Author: Genesy
 */

#include "MMA845x.h"
#include "utility_function.h"
#include "i2cHL.h"
//#include "gpio.h"

_MMA845xQ MMA845xQ;

void MMA845x_init(){
	MMA845xQ.address = ADDR1;
	MMA845xQ_Standby();
//	uint8_t ctrl_reg1_value;
//	I2CRead(MMA845xQ.address, REG_CTRL_REG1, 1, &ctrl_reg1_value);
//	ctrl_reg1_value &= ~REG_CTRL_REG1;
//	I2CWrite(MMA845xQ.address, REG_CTRL_REG1, 1, &ctrl_reg1_value);
//	uint8_t set_ppod = PPOD_MASK;
//	uint8_t set_int_en_drdy = INT_EN_DRDY_MASK;
//	uint8_t set_int_cfg_drdy = INT_CFG_DRDY_MASK;
//	I2CWrite(MMA845xQ.address, REG_CTRL_REG3, 1, &set_ppod);
//	I2CWrite(MMA845xQ.address, REG_CTRL_REG4, 1, &set_int_en_drdy);
//	I2CWrite(MMA845xQ.address, REG_CTRL_REG5, 1, &set_int_cfg_drdy);
//	GPIOInit();
//
//	/* use port2_1 as input event, interrupt test. */
//	GPIOSetDir( PORT2, 1, 0 );
//	/* port2_1, single edge trigger, active high. */
//	GPIOSetInterrupt( PORT2, 1, 0, 0, 0 );
//	GPIOIntEnable( PORT2, 1 );

	MMA845xQ_SetDataRate(ODR_6_25);
	MMA845xQ_SetLowNoise(true);
	MMA845xQ_SetOversamplingMode(HighRes);
}

void MMA845xQ_Standby(){
		uint8_t statusCheck;
		I2CRead(MMA845xQ.address, REG_CTRL_REG1, MMA8451xQ_NADDRESSBYTE, 1, &statusCheck);
		statusCheck &= ~ACTIVE_MASK;
		I2CWrite(MMA845xQ.address, REG_CTRL_REG1, MMA8451xQ_NADDRESSBYTE, 1, &statusCheck);
	return;
}

void MMA845xQ_Active(){
		uint8_t statusCheck;
		I2CRead(MMA845xQ.address, REG_CTRL_REG1, MMA8451xQ_NADDRESSBYTE, 1, &statusCheck);
		statusCheck |= ACTIVE_MASK;
		I2CWrite(MMA845xQ.address, REG_CTRL_REG1, MMA8451xQ_NADDRESSBYTE, 1, &statusCheck);
	return;
}

void MMA845xQ_SetI2CAddress(MMA845x_Address addr){
		MMA845xQ.address = addr;
	return;
}

void MMA845xQ_SetRange(MMA845x_Range res){
		uint8_t data;
		I2CRead(MMA845xQ.address, REG_XYZ_DATA_CFG, MMA8451xQ_NADDRESSBYTE, 1, &data);
		data &= ~FS_MASK;
		data |= res ;
		I2CWrite(MMA845xQ.address, REG_XYZ_DATA_CFG, MMA8451xQ_NADDRESSBYTE, 1, &data);
	return;
}

void MMA845xQ_SetDataRate(MMA845x_DataRate odr){
		uint8_t data;
		odr <<= 3;
		I2CRead(MMA845xQ.address, REG_CTRL_REG1, MMA8451xQ_NADDRESSBYTE, 1, &data);
		data &= ~DR_MASK;
		data |= odr ;
		I2CWrite(MMA845xQ.address, REG_CTRL_REG1, MMA8451xQ_NADDRESSBYTE, 1, &data);
	return;
}

void MMA845xQ_SetLowNoise(bool noise_mod){
		uint8_t data;
		uint8_t n_mod = (uint8_t)noise_mod;
		n_mod <<= 2;
		I2CRead(MMA845xQ.address, REG_CTRL_REG1, MMA8451xQ_NADDRESSBYTE, 1, &data);
		data &= ~LOW_NOISE_MASK;
		data |= n_mod ;
		I2CWrite(MMA845xQ.address, REG_CTRL_REG1, MMA8451xQ_NADDRESSBYTE, 1, &data);
	return;
}

void MMA845xQ_SetOversamplingMode(MMA845x_OsMode os_mode){
		uint8_t data;
		I2CRead(MMA845xQ.address, REG_CTRL_REG2, MMA8451xQ_NADDRESSBYTE, 1, &data);
		data &= ~MODS_MASK;
		data |= os_mode ;
		I2CWrite(MMA845xQ.address, REG_CTRL_REG2, MMA8451xQ_NADDRESSBYTE, 1, &data);
	return;
}

void MMA845xQ_SetHPFCutoff(uint8_t cut_off){
		uint8_t data;
		I2CRead(MMA845xQ.address, REG_HP_FILTER_CUTOFF, MMA8451xQ_NADDRESSBYTE, 1, &data);
		data &= ~SEL_MASK;
		data |= cut_off ;
		I2CWrite(MMA845xQ.address, REG_HP_FILTER_CUTOFF, MMA8451xQ_NADDRESSBYTE, 1, &data);
	return;
}

void MMA845xQ_HPFEnable(){
		uint8_t data;
		I2CRead(MMA845xQ.address, REG_XYZ_DATA_CFG, MMA8451xQ_NADDRESSBYTE, 1, &data);
		data |= HPF_OUT_MASK ;
		I2CWrite(MMA845xQ.address, REG_XYZ_DATA_CFG, MMA8451xQ_NADDRESSBYTE, 1, &data);
	return;
}

void MMA845xQ_HPFDisable(){
		uint8_t data;
		I2CRead(MMA845xQ.address, REG_XYZ_DATA_CFG, MMA8451xQ_NADDRESSBYTE, 1, &data);
		data &= ~HPF_OUT_MASK ;
		I2CWrite(MMA845xQ.address, REG_XYZ_DATA_CFG, MMA8451xQ_NADDRESSBYTE, 1, &data);
	return;
}

uint8_t MMA845xQ_getStatus(){
		I2CRead(MMA845xQ.address, REG_STATUS, MMA8451xQ_NADDRESSBYTE, 1, &(MMA845xQ.status));
	return MMA845xQ.status;
}


void MMA845xQ_getAccelerations(uint8_t* xyz){
		I2CRead(MMA845xQ.address, REG_OUT_X_MSB, MMA8451xQ_NADDRESSBYTE, 6, xyz);
	return;
}

void MMA845xQ_calibration(){
	uint8_t zero_xyz[6];
	MMA845xQ_Standby();
	MMA845xQ_SetDataRate(ODR_200);
	MMA845xQ_Active();
	while((MMA845xQ_getStatus() & 0x8) != 8);
	MMA845xQ_getAccelerations(zero_xyz);
	MMA845xQ_Standby();
	/**************** Metodo 1 *****************
	int16_t zero_xyz_array[3];
	int8_t zero_offset[3];
	uint8_t i;
	for (i = 0; i < 3; i++){
		zero_xyz_array[i] = (int16_t)little_endian_buffer_to_uint16(&zero_xyz[2*i]);
		zero_offset[i] = (int8_t)-(zero_xyz_array[i] / 8);
	}
	zero_offset[2] += 512;
	********************************************/
	/**************** Metodo 1 *****************/
	int16_t x_compl = ((int8_t)zero_xyz[0] * 256) + (int8_t)zero_xyz[1];
	int16_t y_compl = ((int8_t)zero_xyz[2] * 256) + (int8_t)zero_xyz[3];
	int16_t z_compl = ((int8_t)zero_xyz[4] * 256) + (int8_t)zero_xyz[5];
	int8_t zero_offset[3];
	zero_offset[0] = - (x_compl / 8);
	zero_offset[1] = - (y_compl / 8);
	zero_offset[2] = - (4096 - z_compl / 8);
	I2CWrite(MMA845xQ.address, REG_OFF_X, MMA8451xQ_NADDRESSBYTE, 3, (uint8_t*)zero_offset);
	MMA845xQ_getAccelerations(zero_xyz);
	MMA845xQ_SetDataRate(ODR_1_563);
	MMA845xQ_Active();
}
