/*
 * MMA845x.h
 *
 *  Created on: 01/ott/2012
 *      Author: Genesy
 */

#ifndef MMA845X_H_
#define MMA845X_H_


#include "type.h"
#include <stdbool.h>

/*
 * Mappa registri
 */

#define REG_STATUS      		0x00 //(R) Real time status
#define REG_OUT_X_MSB 			0x01 //(R) [7:0] are 8 MSBs of 10-bit sample
#define REG_OUT_X_LSB 			0x02 //(R) [7:6] are 2 LSBs of 10-bit sample
#define REG_OUT_Y_MSB 			0x03 //(R) [7:0] are 8 MSBs of 10-bit sample
#define REG_OUT_Y_LSB 			0x04 //(R) [7:6] are 2 LSBs of 10-bit sample
#define REG_OUT_Z_MSB 			0x05 //(R) [7:0] are 8 MSBs of 10-bit sample
#define REG_OUT_Z_LSB 			0x06 //(R) [7:6] are 2 LSBs of 10-bit sample
#define REG_SYSMOD 				0x0b //(R) Current system mode
#define REG_INT_SOURCE 			0x0c //(R) Interrupt status
#define REG_WHO_AM_I 			0x0d //(R) Device ID (0x3A)
#define REG_XYZ_DATA_CFG 		0x0e //(R/W) Dynamic range settings
#define REG_HP_FILTER_CUTOFF 	0x0f //(R/W) cut-off frequency is set to 16Hz @ 800Hz
#define REG_PL_STATUS 			0x10 //(R) Landscape/Portrait orientation status
#define REG_PL_CFG 				0x11 //(R/W) Landscape/Portrait configuration
#define REG_PL_COUNT 			0x12 //(R) Landscape/Portrait debounce counter
#define REG_PL_BF_ZCOMP 		0x13 //(R) Back-Front, Z-Lock trip threshold
#define REG_P_L_THS_REG 		0x14 //(R/W) Portrait to Landscape trip angle is 29 degree
#define REG_FF_MT_CFG 			0x15 //(R/W) Freefall/motion functional block configuration
#define REG_FF_MT_SRC 			0x16 //(R) Freefall/motion event source register
#define REG_FF_MT_THS 			0x17 //(R/W) Freefall/motion threshold register
#define REG_FF_MT_COUNT 		0x18 //(R/W) Freefall/motion debounce counter
#define REG_TRANSIENT_CFG 		0x1d //(R/W) Transient functional block configuration
#define REG_TRANSIENT_SRC 		0x1e //(R) Transient event status register
#define REG_TRANSIENT_THS 		0x1f //(R/W) Transient event threshold
#define REG_TRANSIENT_COUNT 	0x20 //(R/W) Transient debounce counter
#define REG_PULSE_CFG 			0x21 //(R/W) ELE, Double_XYZ or Single_XYZ
#define REG_PULSE_SRC 			0x22 //(R) EA, Double_XYZ or Single_XYZ
#define REG_PULSE_THSX 			0x23 //(R/W) X pulse threshold
#define REG_PULSE_THSY 			0x24 //(R/W) Y pulse threshold
#define REG_PULSE_THSZ 			0x25 //(R/W) Z pulse threshold
#define REG_PULSE_TMLT 			0x26 //(R/W) Time limit for pulse
#define REG_PULSE_LTCY 			0x27 //(R/W) Latency time for 2nd pulse
#define REG_PULSE_WIND 			0x28 //(R/W) Window time for 2nd pulse
#define REG_ASLP_COUNT 			0x29 //(R/W) Counter setting for auto-sleep
#define REG_CTRL_REG1 			0x2a //(R/W) ODR = 800 Hz, STANDBY mode
#define REG_CTRL_REG2 			0x2b //(R/W) Sleep enable, OS Modes, RST, ST
#define REG_CTRL_REG3 			0x2c //(R/W) Wake from sleep, IPOL, PP_OD
#define REG_CTRL_REG4 			0x2d //(R/W) Interrupt enable register
#define REG_CTRL_REG5 			0x2e //(R/W) Interrupt pin (INT1/INT2) map
#define REG_OFF_X 				0x2f //(R/W) X-axis offset adjust
#define REG_OFF_Y 				0x30 //(R/W) Y-axis offset adjust
#define REG_OFF_Z 				0x31 //(R/W) Z-axis offset adjust


#define ACTIVE_MASK 			0x01
#define FS_MASK					0x03
#define MODS_MASK				0x03
#define SEL_MASK				0x03
#define HPF_OUT_MASK			0x10
#define FREAD_MASK				0x02
#define PPOD_MASK				0x01
#define INT_EN_DRDY_MASK		0x01
#define INT_CFG_DRDY_MASK       0x01
#define DR_MASK					0x38      //0b00111000 -- Seleziono DR2-DR0 in CTRL_REG1
#define ZYXDR_FLAG_MASK			0x08	  //indicates new data available for any axis
#define LOW_NOISE_MASK			0x04

#define MMA8451xQ_RESOLUTION 	14

#define MMA8451xQ_NADDRESSBYTE	1

typedef enum { ADDR1 = 0x38, ADDR2 = 0x3A } MMA845x_Address ;
typedef enum { FULL_SCALE_RANGE_2g = 0x00, FULL_SCALE_RANGE_4g = 0x01, FULL_SCALE_RANGE_8g = 0x2 } MMA845x_Range ;
typedef enum { ODR_800, ODR_400, ODR_200, ODR_100, ODR_50, ODR_12_5, ODR_6_25, ODR_1_563 } MMA845x_DataRate ;
typedef enum { Normal, LowSNR_LowPw, HighRes, LowPw} MMA845x_OsMode;

typedef struct {
	uint8_t address;
	uint8_t status;
}_MMA845xQ;




//inizializza l'accelerometro con la configurazione di default (da salvare in flash TODO)
void MMA845x_init();
/*
** Go to Standby Mode
*/
void MMA845xQ_Standby();
/*
** Go to Active Mode
*/
void MMA845xQ_Active();

void MMA845xQ_SetI2CAddress(MMA845x_Address addr);

void MMA845xQ_SetRange(MMA845x_Range res);

void MMA845xQ_SetDataRate(MMA845x_DataRate odr);

void MMA845xQ_SetLowNoise(bool noise_mod);

void MMA845xQ_SetOversamplingMode(MMA845x_OsMode os_mode);

void MMA845xQ_SetHPFCutoff(uint8_t cut_off);

void MMA845xQ_HPFEnable();

void MMA845xQ_HPFDisable();

//per la gestione a polling
uint8_t MMA845xQ_getStatus();

void MMA845xQ_getAccelerations(uint8_t* xyz);

void MMA845xQ_calibration();




#endif /* MMA845X_H_ */
