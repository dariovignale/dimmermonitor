/*
 * MMA845x_stub.c
 *
 *  Created on: 23/apr/2013
 *      Author: Genesy
 */

#include "MMA845x.h"
#include "utility_function.h"
#include "i2cHL.h"
//#include "gpio.h"

_MMA845xQ MMA845xQ;

void MMA845x_init(){

}

void MMA845xQ_Standby(){

	return;
}

void MMA845xQ_Active(){

	return;
}

void MMA845xQ_SetI2CAddress(MMA845x_Address addr){
		MMA845xQ.address = addr;
	return;
}

void MMA845xQ_SetRange(MMA845x_Range res){

	return;
}

void MMA845xQ_SetDataRate(MMA845x_DataRate odr){

	return;
}

void MMA845xQ_SetLowNoise(bool noise_mod){

	return;
}

void MMA845xQ_SetOversamplingMode(MMA845x_OsMode os_mode){

	return;
}

void MMA845xQ_SetHPFCutoff(uint8_t cut_off){

	return;
}

void MMA845xQ_HPFEnable(){

	return;
}

void MMA845xQ_HPFDisable(){

	return;
}

uint8_t MMA845xQ_getStatus(){
		//I2CRead(MMA845xQ.address, REG_STATUS, MMA8451xQ_NADDRESSBYTE, 1, &(MMA845xQ.status));
	return 1;
}


void MMA845xQ_getAccelerations(uint8_t* xyz){
		//I2CRead(MMA845xQ.address, REG_OUT_X_MSB, MMA8451xQ_NADDRESSBYTE, 6, xyz);
	*xyz = 100;
	return;
}

void MMA845xQ_calibration(){

}
