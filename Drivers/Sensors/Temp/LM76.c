/*
 * LM76.c
 *
 *  Created on: 08/nov/2012
 *      Author: Genesy
 */

#include "LM76.h"
#include "utility_function.h"
#include <string.h>
//#include "lite_printf.h"


uint8_t LM76_GetTemp(){
	uint8_t buffer[2] = {0, 0};
	uint8_t state = I2CRead(LM76_ADDR, 0, LM76_NBYTEADDRESS, 2, buffer);
	uint16_t data = little_endian_buffer_to_uint16(buffer);
	memcpy(&temp_reg, &data, sizeof(temp_reg));
	return state;
}


void LM76_Config(){
	uint8_t config = 0x60;
	I2CWrite(LM76_ADDR, 0, LM76_NBYTEADDRESS, 1, &config);
	return;
}


float LM76_Convert(){
	int16_t tmp = 0;
	memcpy(&tmp, &temp_reg, sizeof(temp_reg));
	uint16_t module = two_complement_To_module_conv(tmp, LM76_RES);
	float value = module * 0.0625;
	return value;
}

/*
void print_temp(){
	lite_printf("Temp:  %c%-3.4f", temp_reg.sign ? '-' : '+', LM76_Convert());
}
*/
