/*
 * LM76.h
 *
 *  Created on: 08/nov/2012
 *      Author: Genesy
 */

#ifndef LM76_H_
#define LM76_H_

#include <stdint.h>
//#include <string.h>
#include "i2cHL.h"

#define LM76_NBYTEADDRESS	1

/* For more info, read Philips's LM95 datasheet */
#define LM76_ADDR		0x90
#define LM76_TEMP		0x00
#define LM76_CONFIG		0x01
#define LM76_THYST		0x02
#define LM76_TOS		0x03

#define LM76_RES        13


typedef struct {
	uint16_t status:3;
	uint16_t temperature:12;
	uint16_t sign:1;
} temp_reg_t;

temp_reg_t temp_reg;

uint8_t LM76_GetTemp();
void LM76_Config();
float LM76_Convert();
//void print_temp();


#endif /* LM76_H_ */
