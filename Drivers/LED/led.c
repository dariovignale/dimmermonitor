/*
 * led.c
 *
 *  Created on: 14/nov/2012
 *      Author: Genesy
 */

#include "led.h"
#include <stdbool.h>
#include "driver_config.h"
#include "gpio.h"

void LED_Toggle(uint32_t portNum, uint32_t bitPosi){
	bool led_status = GPIOGetValue( portNum, bitPosi );
	GPIOSetValue( portNum, bitPosi, !led_status );
	return;
}
