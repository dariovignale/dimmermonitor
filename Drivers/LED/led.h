/*
 * led.h
 *
 *  Created on: 14/nov/2012
 *      Author: Genesy
 */

#ifndef LED_H_
#define LED_H_

#include <stdint.h>

void LED_Toggle(uint32_t portNum, uint32_t bitPosi);

#endif /* LED_H_ */
