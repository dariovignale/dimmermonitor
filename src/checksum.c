/*
 * checksum.c
 *
 *  Created on: 01/feb/2013
 *      Author: Genesy
 */

#include "checksum.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


uint8_t calcCksum (uint8_t *string, uint16_t len)
{
  volatile uint8_t checksum = 0;
  volatile uint16_t i = 0;
  while ( i < len){
	  checksum += (uint8_t)string[i];
	  i++;
  }
  checksum = ~checksum;
  return checksum;
}

