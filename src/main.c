/*
===============================================================================
 Name        : main.c
 Author      : 
 Version     :
 Copyright   : Copyright (C) 
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>

// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

#include "core_cm0.h"
#include "driver_config.h"
#include "target_config.h"
#include "type.h"
#include "pwm.h"
#include "gpio.h"
#include "uart.h"
#include "parsing.h"
#include "pwmDutyTable.h"
#include "gpio.h"
#include "i2c.h"
#include <stdint.h>



#define CNT_PIN_PORT			1
#define CNT_PIN_BIT				5

#define CALIB_MODE_PIN_PORT		1
#define CALIB_MODE_PIN_BIT		11


typedef union  {
	uint8_t ds_value;
	struct {
		uint8_t ds_b1:1;
		uint8_t ds_b2:1;
		uint8_t ds_b3:1;
		uint8_t	   	:5;
	};
} DipSwitch_t;


#define NVIC_PRIORITY_LOWEST   ((1<<__NVIC_PRIO_BITS) - 1)
#define NVIC_PRIORITY_LOWER    ((1<<__NVIC_PRIO_BITS) - 2)


volatile uint8_t my_addr;
volatile uint8_t jmp_value;

int main(void) {
	SystemCoreClockUpdate();
	GPIOInit();
	GPIOSetDir( CNT_PIN_PORT, CNT_PIN_BIT,  1);
	GPIOSetValue(CNT_PIN_PORT, CNT_PIN_BIT, 0);
	GPIOSetDir( CALIB_MODE_PIN_PORT, CALIB_MODE_PIN_BIT,  0);
	GPIOSetValue(CALIB_MODE_PIN_PORT, CALIB_MODE_PIN_BIT, 1);
	DipSwitch_t ds1;
	ds1.ds_value = 0;
	ds1.ds_b1 = GPIOGetValue(1, 10);
	ds1.ds_b2 = GPIOGetValue(1, 9);
	ds1.ds_b3 = GPIOGetValue(1, 8);
	my_addr = ds1.ds_value;
	jmp_value = GPIOGetValue(CALIB_MODE_PIN_PORT, CALIB_MODE_PIN_BIT);

	NVIC_SetPriority( I2C_IRQn, NVIC_PRIORITY_LOWER  );
	NVIC_SetPriority( UART_IRQn, NVIC_PRIORITY_LOWEST );

	UART_Init(UART_BAUD);
	I2CInit();

	initPWMDutyCycleLookup();
	pwmInit();




	while (1) {
		__WFI();
	}

	return 0 ;
}
