/*
 * pwm.h
 *
 *  Created on: 31/mag/2013
 *      Author: Fabio
 */

#ifndef PWM_H_
#define PWM_H_

#include <stdint.h>


void pwmInit();

void pwmSetDutyCycle(uint16_t duty_cycle);

void pwmSetFreq(uint32_t pwm_freq);

#endif /* PWM_H_ */
