/*
 * pwmDutyTable.h
 *
 *  Created on: 05/lug/2013
 *      Author: Fabio
 */

#ifndef PWMDUTYTABLE_H_
#define PWMDUTYTABLE_H_

#include <stdint.h>
#include <stdbool.h>

void initPWMDutyCycleLookup();

uint16_t getPWMDutyCycleElem(uint16_t i);

bool getInverted();

void setInverted(bool value);


#endif /* PWMDUTYTABLE_H_ */
