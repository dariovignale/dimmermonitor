/*
 * EEPROM.h
 *
 *  Created on: 23/ott/2012
 *      Author: Genesy
 */

#ifndef EEPROM_H_
#define EEPROM_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "i2cHL.h"
#include "target_config.h"


#define READ_WRITE			0x01
#define MARKER_VALUE		0xABCD
static const uint8_t RESET_VALUE_8 = 0xFF;
static const uint16_t RESET_VALUE_16 = 0xFFFF;

typedef struct __attribute__ ((packed))
 {
	 uint16_t marker;
	 uint16_t dimmerTable[256];
	 uint8_t  pwm_freq;
	 bool     inverted;
 }EEPROMLayout;


#define EEPROMLocation(x) offsetof(EEPROMLayout, x)
#define SIZEOF(x) ((size_t) sizeof(((EEPROMLayout *)0)->x))

#define EEPROMRead(member, data) I2CRead(MEMORY_ADDR, EEPROMLocation(member), EEPROM_NADDRESSBYTE, SIZEOF(member), (uint8_t*)&data);

#define EEPROMWrite(member, data) I2CWrite(MEMORY_ADDR, EEPROMLocation(member), EEPROM_NADDRESSBYTE, SIZEOF(member), (uint8_t*)&data);

void EEPROM_delayBetweenOp();

#endif /* EEPROM_H_ */

