#include "pwmDutyTable.h"
#include "EEPROM.h"
#include <string.h>

#define DEFAULT_PWM_VALUE 			2000


static uint16_t pwmDutyCycleLookup[256];
static bool inverted = false;

void initPWMDutyCycleLookup() {
	uint16_t i = 0;
	uint16_t markerValue;

	for (i = 0; i < 256; i++) {
		pwmDutyCycleLookup[i] = 2000;
	}

	EEPROMRead(marker, markerValue);

	if (markerValue == MARKER_VALUE) {
		for (i = 0; i < 256; i++) {
			EEPROMRead(dimmerTable[i], pwmDutyCycleLookup[i]);
		}
	}

	EEPROMRead(inverted, inverted);
}

uint16_t getPWMDutyCycleElem(uint16_t i) {
	if(inverted){
		return 4000 - pwmDutyCycleLookup[i];
	}
	else {
		return pwmDutyCycleLookup[i];
	}
}

bool getInverted(){
	return inverted;
}

void setInverted(bool value){
	inverted = value;
}
