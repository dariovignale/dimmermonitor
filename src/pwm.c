/*
 * pwm.c
 *
 *  Created on: 31/mag/2013
 *      Author: Fabio
 */

#include "pwm.h"
#include "timer32.h"
#include "pwmDutyTable.h"

#ifdef __USE_CMSIS
#include "LPC11xx.h"
#endif

#include "core_cm0.h"
#include "EEPROM.h"

#define MIN_PWM_FREQ 			80
#define MAX_PWM_FREQ 			250

static const uint32_t pwm_step = 4000;
static uint8_t pwm_freq = MIN_PWM_FREQ;


static uint32_t period;

//static const uint32_t period = 192000;		//about 250Hz PWM frequency (duty_cicle = 1: 1uS - duty_cicle = 4000 = 4ms)


//static const uint32_t period = 259200;		//about 184Hz PWM frequency (duty_cicle = 1: 1,35uS - duty_cicle = 4000 = 5,4ms)


//static const uint32_t period = 768000;		//about 62.5Hz PWM frequency (duty_cicle = 1: 4uS - duty_cicle = 4000 = 16ms)




#define INITIAL_PWM_STEP		128 				// 50%


typedef struct {
	uint16_t duty_cycle;
}t_pwm_state;

t_pwm_state pwm_state;

static void readSavedPWMFreq() {
	uint16_t markerValue;
	EEPROMRead(marker, markerValue);
	if (markerValue == MARKER_VALUE) {
		uint8_t readedPwmFreq = 0;
		EEPROMRead(pwm_freq, readedPwmFreq);
		if (readedPwmFreq >= MIN_PWM_FREQ && readedPwmFreq <= MAX_PWM_FREQ) {
			pwm_freq = readedPwmFreq;
		}
	}
}

void pwmInit() {
	readSavedPWMFreq();
	pwmSetDutyCycle(getPWMDutyCycleElem(INITIAL_PWM_STEP));
	pwmSetFreq(pwm_freq);
}

void pwmSetFreq(uint32_t pwm_freq){
	period = SystemCoreClock / pwm_freq;
	init_timer32PWM(1, period, 1);
	enable_timer32(1);
	pwmSetDutyCycle(pwm_state.duty_cycle);
}

void pwmSetDutyCycle(uint16_t duty_cycle) {
	pwm_state.duty_cycle = duty_cycle;
	setMatch_timer32PWM(1, 0, period * duty_cycle / pwm_step);
}

