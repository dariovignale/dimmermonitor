/*
 * nmea.c
 *
 *  Created on: 31/gen/2013
 *      Author: Genesy
 */

#include "parsing.h"
#include "checksum.h"
#include "uart.h"
#include "pwm.h"
#include "pwmDutyTable.h"
#include "EEPROM.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define S_WAITING_BELL			0
#define S_WAITING_LENGTH		1
#define S_WAITING_BRT_DATA		2
#define S_WAITING_STB_DATA		3
#define S_WAITING_RTB_DATA		4
#define S_WAITING_SPW_DATA		5
#define S_WAITING_INV_DATA		6

#define ASCII_BELL						0x07
#define BUFFER_SIZE						522
#define MSG_COMMAND_LENGTH				3
#define HEADER_LENGTH 					7
#define TOTAL_COMMAND_LENGTH  			9
#define BROADCAST_ADDR 					0xFF

static const char* BRT_CMD				 = "BRT";
static const char* SET_TABLE_CMD 		 = "STB";
static const char* RESET_TABLE_CMD		 = "RTB";
static const char* SET_PWM_FREQ_CMD 	 = "SPW";
static const char* SET_INV_CMD			 = "INV";



extern volatile uint32_t UARTCount;
extern volatile uint8_t UARTBuffer[UART_BUFSIZE];
extern volatile uint8_t my_addr;
extern volatile uint8_t jmp_value;

int8_t status = S_WAITING_BELL;

uint8_t sentenceBuf[BUFFER_SIZE];
uint32_t nTotalRead = 0;

typedef struct {
	;
	uint8_t monitor_addr;
	char command[4];
	uint8_t data_len;
	union {
		uint8_t data[2];
		uint16_t dataUInt16;
	};
} Dimmer_cmd_t;

Dimmer_cmd_t m_dimmerStbHeader = { .command = "\0" };

static bool CheckChecksum(uint8_t* sentenceBuf, uint8_t read_ckm, uint16_t len);
static void parseHeader(void);

bool CheckChecksum(uint8_t* sentenceBuf, uint8_t read_ckm, uint16_t len) {
	uint8_t calc_ckm = 0;
	bool bRet = false;

	//evita di sporcare la sentenza arrivata
	uint8_t sentence[BUFFER_SIZE];
	memcpy(sentence, sentenceBuf, BUFFER_SIZE);

	calc_ckm = calcCksum(sentence, len);

	if (calc_ckm == read_ckm) {
		bRet = true;
	}
	return bRet;
}

static bool IsMyAddr() {
	bool retVal = false;
	if ((m_dimmerStbHeader.monitor_addr == my_addr)
			|| (m_dimmerStbHeader.monitor_addr == BROADCAST_ADDR))
		retVal = true;
	return retVal;
}

uint32_t readCommand() {

	uint16_t i = 0;
	uint32_t retval;
	uint16_t markerValue;

	switch (status) {

		case S_WAITING_BELL:
			if (UARTBuffer[UARTCount - 1] == ASCII_BELL) {
				memset(sentenceBuf, 0, sizeof(sentenceBuf));
				sentenceBuf[0] = UARTBuffer[UARTCount - 1];
				nTotalRead = 1;
				status = S_WAITING_LENGTH;
			} else {
				UARTCount = 0;
				status = S_WAITING_BELL;
			}
			break;

		case S_WAITING_LENGTH:

			if (nTotalRead < HEADER_LENGTH) {
				sentenceBuf[nTotalRead] = UARTBuffer[UARTCount - 1];
				nTotalRead++;
			}

			if (nTotalRead == HEADER_LENGTH) {
				if (CheckChecksum(sentenceBuf, sentenceBuf[nTotalRead - 1],
						nTotalRead - 1) == false) {
					status = S_WAITING_BELL;
				} else {
					parseHeader();

					if (IsMyAddr()) {
						if (strcmp(m_dimmerStbHeader.command, BRT_CMD) == 0) {
							status = S_WAITING_BRT_DATA;
						}
						else if ((strcmp(m_dimmerStbHeader.command, SET_TABLE_CMD)
								== 0) && jmp_value == 0) {
							status = S_WAITING_STB_DATA;
						}
						else if ((strcmp(m_dimmerStbHeader.command, RESET_TABLE_CMD)
										== 0) && jmp_value == 0) {
							status = S_WAITING_RTB_DATA;
						}
						else if ((strcmp(m_dimmerStbHeader.command, SET_PWM_FREQ_CMD)
								== 0) && jmp_value == 0) {
							status = S_WAITING_SPW_DATA;
						}
						else if ((strcmp(m_dimmerStbHeader.command, SET_INV_CMD)
								== 0) && jmp_value == 0) {
							status = S_WAITING_INV_DATA;
						}
						else {
							UARTCount = 0;
							status = S_WAITING_BELL;
						}
					} else {
						UARTCount = 0;
						status = S_WAITING_BELL;
					}
			}
		}

		break;

		case S_WAITING_BRT_DATA:
			if (nTotalRead < HEADER_LENGTH + m_dimmerStbHeader.data_len + 1) {
				sentenceBuf[nTotalRead] = UARTBuffer[UARTCount - 1];
				nTotalRead++;
			}
			if (nTotalRead == HEADER_LENGTH + m_dimmerStbHeader.data_len + 1) {
				memcpy(m_dimmerStbHeader.data, &sentenceBuf[HEADER_LENGTH],
						m_dimmerStbHeader.data_len);
				if (CheckChecksum(&sentenceBuf[HEADER_LENGTH],
						sentenceBuf[nTotalRead - 1],
						m_dimmerStbHeader.data_len) != false) {
					uint16_t data = m_dimmerStbHeader.dataUInt16;
					if (jmp_value == 0 && data <= 4000) {
						if(getInverted()){
							pwmSetDutyCycle(4000 - data);
						}
						else {
							pwmSetDutyCycle(data);
						}
					} else {
							pwmSetDutyCycle(getPWMDutyCycleElem((uint8_t) data));
					}
				}
				UARTCount = 0;
				status = S_WAITING_BELL;
			}

			break;

		case S_WAITING_STB_DATA:
			if (nTotalRead
					< HEADER_LENGTH + ((m_dimmerStbHeader.data_len + 2) * 2) + 1) {
				sentenceBuf[nTotalRead] = UARTBuffer[UARTCount - 1];
				nTotalRead++;
			}
			if (nTotalRead
					== HEADER_LENGTH + ((m_dimmerStbHeader.data_len + 2) * 2) + 1) {

					if (CheckChecksum(&sentenceBuf[HEADER_LENGTH],
							sentenceBuf[nTotalRead - 1],
							((m_dimmerStbHeader.data_len + 2) * 2)) != false) {

						for (i = 0; i < m_dimmerStbHeader.data_len + 1; i++) {
							retval = EEPROMWrite(dimmerTable[i],
									sentenceBuf[HEADER_LENGTH + (i*2) + 2]);

							//only for test
							if (retval != I2C_OK) {
								i = 0xFF;
							}

							EEPROM_delayBetweenOp();
						}

						retval = EEPROMWrite(pwm_freq,
											 sentenceBuf[HEADER_LENGTH]);

						EEPROM_delayBetweenOp();

						retval = EEPROMWrite(inverted,
											 sentenceBuf[HEADER_LENGTH + 1]);

						EEPROM_delayBetweenOp();

						markerValue = MARKER_VALUE;
						EEPROMWrite(marker, markerValue);
						EEPROM_delayBetweenOp();
					}

					//retval = I2CWrite(MEMORY_ADDR, 0, 2, 50, data);

					//			if (retval != I2C_OK){
					//				i=0xFF;
					//			}

					UARTCount = 0;
					status = S_WAITING_BELL;
				}
			break;

		case S_WAITING_RTB_DATA:


			retval = EEPROMWrite(marker, RESET_VALUE_16);
			EEPROM_delayBetweenOp();

			for (i = 0; i <= 512; i++) {
				retval = EEPROMWrite(dimmerTable[i], RESET_VALUE_8);

				if (retval != I2C_OK) {
					i = 0xFF;
				}

				EEPROM_delayBetweenOp();
			}

			UARTCount = 0;
			status = S_WAITING_BELL;

			break;

		case S_WAITING_SPW_DATA:

			if (nTotalRead
					< HEADER_LENGTH + m_dimmerStbHeader.data_len + 1) {
				sentenceBuf[nTotalRead] = UARTBuffer[UARTCount - 1];
				nTotalRead++;
			}
			if (nTotalRead
					== HEADER_LENGTH + m_dimmerStbHeader.data_len + 1) {

					if (CheckChecksum(&sentenceBuf[HEADER_LENGTH],
							sentenceBuf[nTotalRead - 1],
							m_dimmerStbHeader.data_len) != false) {

						uint8_t pwmFreq = sentenceBuf[HEADER_LENGTH];
						//pwmSetFreq(pwmFreq);

						EEPROMWrite(pwm_freq, pwmFreq);
						EEPROM_delayBetweenOp();
					}

					UARTCount = 0;
					status = S_WAITING_BELL;
				}
			break;

		case S_WAITING_INV_DATA:

			if (nTotalRead
					< HEADER_LENGTH + m_dimmerStbHeader.data_len + 1) {
				sentenceBuf[nTotalRead] = UARTBuffer[UARTCount - 1];
				nTotalRead++;
			}
			if (nTotalRead
					== HEADER_LENGTH + m_dimmerStbHeader.data_len + 1) {

					if (CheckChecksum(&sentenceBuf[HEADER_LENGTH],
							sentenceBuf[nTotalRead - 1],
							m_dimmerStbHeader.data_len) != false) {

						bool inv = sentenceBuf[HEADER_LENGTH];
						setInverted(inv);

						EEPROMWrite(inverted, inv);
						EEPROM_delayBetweenOp();
					}

					UARTCount = 0;
					status = S_WAITING_BELL;
				}
			break;

	}
	return nTotalRead;
}

void parseHeader() {
	m_dimmerStbHeader.monitor_addr = sentenceBuf[1];
	m_dimmerStbHeader.data_len = sentenceBuf[5];
	memcpy(&m_dimmerStbHeader.command, &sentenceBuf[2], MSG_COMMAND_LENGTH);
}

