/*
 * checksum.h
 *
 *  Created on: 01/feb/2013
 *      Author: Genesy
 */

#ifndef CHECKSUM_H_
#define CHECKSUM_H_

#include <stdint.h>

uint8_t calcCksum (uint8_t *string, uint16_t len);

#endif /* CHECKSUM_H_ */
