/*
 * EEPROM.c
 *
 *  Created on: 11/gen/2013
 *      Author: Genesy
 */

#include "EEPROM.h"


#pragma GCC push_options
#pragma GCC optimize ("O0")


void EEPROM_delayBetweenOp(){
	volatile int i;
	for ( i = 0; i < 0x3000; i++ ) {};
}

#pragma GCC pop_options
