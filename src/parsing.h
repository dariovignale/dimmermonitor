/*
 * parsing.h
 *
 *  Created on: 31/gen/2013
 *      Author: Genesy
 */

#ifndef PARSING_H_
#define PARSING_H_

#include <stdint.h>

uint32_t readCommand();

#endif /* PARSING_H_ */
