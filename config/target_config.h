/*****************************************************************************
 *   target_config.h:  config file for blinky example for NXP LPC11xx Family
 *   Microprocessors
 *
 *   Copyright(C) 2008, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2010.09.07  ver 1.00    Preliminary version, first Release
 *
******************************************************************************/


#define UART_BAUD 9600


#define MEMORY_ADDR		0xA0
#define EEPROM_NADDRESSBYTE		2


/*********************************************************************************
**                            End Of File
*********************************************************************************/
